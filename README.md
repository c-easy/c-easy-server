# C-Easy Server

Ce dépôt contient le code du serveur de la plateforme didactique [C-Easy](https://gitlab.com/c-easy).

## Dépendances

Le serveur utilise les paquets suivants : 
*  Express,
*  express-enforces-ssl,
*  Multer,
*  Helmet,
*  Argon2,
*  Tree-Kill,
*  jsonwebtoken,
*  Bunyan,
*  Jest.

Pour installer les dépendances, placez vous à la racine du répertoire du projet et exécutez la commande suivante :
```
npm install
```

## Consignes


Avant de lancer le serveur, pour s'assurer du bon fonctionnement de ce dernier, il faut créer les quatre répertoires suivant à la racine du répertoire :
*  `tmp/`, ce répertoire contiendra temporairement le code C et l'exécutable lors du l'exécution de la fonction `compileAndTest`,
*  `log/`, ce répertoire contiendra les logs du serveur (fonctionnalité à améliorer),
*  `uploads/`, ce répertoire contiendra les PDFs des différents cours,
*  `key/`, dans ce répertoire vous placerez votre clé (`key.pem`) et votre certificat SSL (`certificate.cert`), sans ces derniers, le serveur ne pourra pas s'exécuter.

Vous pouvez changer la configuration du serveur (adresse de la base de données, système de journalisation, port, adresse, ...) en allant dans `config/` et en créant les trois fichiers suivant :
*  `server-config.json` en vous inspirant de `server-config.json.default`,
*  `database-config.json` en vous inspirant de `database-config.json.default`,
*`  log-config.json` en vous inspirant de `log-config.json.default`.

Vous pouvez aussi exécuter les tests à l'aide de la commande :
```
npm run test
```

## Exécuter le serveur

Pour exécuter le serveur, il suffit d'utiliser la commande :
```
npm start
```

>  **Si vous utilisez des certificats self-signed, n'oubliez pas de les autoriser dans votre navigateur !**