const { execute } = require('./execute');
const fs = require('fs');
const {logger} = require('../lib/logger');
const compiler = "gcc";
const executableExt = ".out";

module.exports = {
    /**
     * Compiles the file in argument then return the executable filepath or the compilation errors
     * @param {String} filepath 
     */
    compileC: async function(filepath){
        let executableName = filepath.replace('.c', executableExt);
        let result = await execute(compiler, ["-Wall", "-Werror", filepath, "-o", executableName]);
        fs.unlink(filepath, function(err){
            logger.debug(err);
        });
        if(result.code != 0){
            return result;
        } else {
            return executableName;
        }
    }
}
