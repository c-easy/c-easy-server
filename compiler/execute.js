const { logger } = require('../lib/logger'),
    child_process = require('child_process'),
    tree_kill = require('tree-kill'),
    TIMEOUT = 5000;

module.exports = {
    /**
     * Executes the command in argument with the arguments in the Array args.
     * @param {String} cmd 
     * @param {Array} args 
     */
    execute: function (cmd, args) {
        if (args && args instanceof Array) {
            let start = process.hrtime();
            const child = child_process.spawn(cmd, args);

            let streams = {
                stdout: "",
                stderr: ""
            };

            let timeout = setTimeout(function(){
                logger.warn(cmd + " reached the timeout after " + TIMEOUT + "ms");
                tree_kill(child.pid);
                streams.stderr = "I killed the sheriff";
            }, TIMEOUT);

            child.stdout.on('data', function (data) {
                streams.stdout += data.toString();
                // logger.debug("Data found in stdout", data);
            });

            child.stdout.on('error', function (error) {
                // logger.error("Error with stdout", error);
            });

            child.stderr.on('data', function (data) {
                streams.stderr += data.toString();
                // logger.debug("Data found in stderr", data.toString());
            });

            child.stderr.on('error', function (error) {
                // logger.error("Error with stderr", error);
            });

            return new Promise(function (resolve, reject) {
                child.on('close', function (code) {
                    if (timeout != undefined) 
                        clearTimeout(timeout);
                    // end[0] = heures, end[1] / 1000000 = ms
                    let end = process.hrtime(start);
                    let result = {
                        code: code,
                        stdout: streams.stdout,
                        stderr: streams.stderr,
                        execution_time: end[1] / 1000000
                    };
                    logger.debug("Command " + cmd + " executed with the following arguments " + args, result);
                    resolve(result);
                });

                child.on('error', function (error) {
                    logger.debug("Execution error with " + cmd + " with the following arguments " + args, error);
                    reject(error);
                });
            });
        }
    }
}

module.exports.TIMEOUT = TIMEOUT;