const { writeSource } = require('./write');
const { compileC } = require('./compile');
const { execute } = require('./execute');
const { logger } = require('../lib/logger')
const fs = require('fs');

module.exports = {
    /**
     * Compiles then test the executable of the source code.
     * @param {String} source 
     * @param {Array} testSet 
     */
    compileAndTest: async function (source, testSet) {
        if (source && testSet && testSet instanceof Array) {
            let filepath = await writeSource(source);
            let result = await compileC(filepath);
            if (result instanceof Object) {
                return result;
            } else {
                let completedTestSet = [];
                for(var test of testSet){
                    if (test && test instanceof Object && !(test instanceof Array)) {
                        if (test.input) {
                            let input = test.input.toString().split(' ').filter(function (el) {return el != "";});
                            test = {
                                code: "",
                                input: test.input,
                                expected: test.expected,
                                output: "",
                                execution_time: ""
                            }
                            let exec = await execute(result, input);
                            if (exec.code == 0) {
                                test.code = exec.code;
                                test.output = exec.stdout;
                                test.execution_time = exec.execution_time;
                            } else {
                                test.code = exec.code;
                                let error = new Error("The program was killed");
                                fs.unlink(result, function (err) {
                                    if (err) {
                                        logger.debug(err);
                                        throw new Error("The executable was not destroyed.");
                                    }
                                });
                                logger.debug(error);
                                throw error;
                            }
                        } else {
                            test.code = -1;
                            let error = new Error(JSON.stringify(test) + " has no input, please use the correct syntax");
                            fs.unlink(result, function (err) {
                                if (err) {
                                    logger.debug(err);
                                    throw new Error("The executable was not destroyed.");
                                }
                            });
                            logger.debug(error);
                            throw error;
                        }
                    } else {
                        let error = new Error(test + " is not an object, please use the correct syntax");
                        fs.unlink(result, function (err) {
                            if (err) {
                                logger.debug(err);
                                throw new Error("The executable was not destroyed.");
                            }
                        });
                        logger.debug(error);
                        throw error;
                    }
                    completedTestSet.push(test);
                }
                fs.unlink(result, function (err) {
                    if (err) {
                        logger.debug(err);
                        throw new Error("The executable was not destroyed.");
                    }
                });
                result = {
                    testSet: completedTestSet
                }
                return result;
            }
        } else {
            let error = new Error("testSet should be an array of Test");
            logger.debug(error);
            throw error;
        }
    }
}