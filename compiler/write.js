const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

const tmpFolder = './tmp';

module.exports = {
    /**
     * Writes the source code in a file then return the filepath
     * @param {String} source 
     */
    writeSource: function(source) {
        try {
            if(!fs.existsSync(tmpFolder)){
                fs.mkdirSync(tmpFolder);
            }
            const filename = crypto.randomBytes(20).toString('hex') + ".c";
            const filepath = path.join(tmpFolder, filename);
            return new Promise(function(resolve, reject){
                fs.writeFile(filepath, source, function errorCallback(error){
                    if(error){
                        reject(error);
                    } else {
                        resolve(filepath);
                    }
                });
            });
        } catch(err){
            throw err;
        }
    }
}

module.exports.tmpFolder = tmpFolder;
