const fs = require('fs'),
      path = require('path');

const databaseConfigPath = "database-config.json";
const logConfigPath = "log-config.json";
const serverConfigPath = "server-config.json";

/**
 * Reads the JSON indicated by the filepath in argument.
 * @param {String} filepath 
 */
function importConf(filepath){
    let conf;
    try {
        conf = fs.readFileSync(path.join(__dirname, filepath));
        return JSON.parse(conf);
    } catch( error ){
        console.log("Couldn't find " + filepath + ", I will use the default config file.");
        conf = fs.readFileSync(path.join(__dirname, filepath + ".default"));
        return JSON.parse(conf);
    }
}

module.exports.databaseConfig = importConf(databaseConfigPath);
module.exports.logConfig = importConf(logConfigPath);
module.exports.serverConfig = importConf(serverConfigPath);