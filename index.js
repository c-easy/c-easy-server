const {startServer} = require('./server/server');
const {serverConfig} = require('./config/config');

startServer(serverConfig.host, serverConfig.port);
