const bunyan = require('bunyan');
const {logConfig} = require('../config/config');

const logger = bunyan.createLogger({
    name: 'c-easy',
    streams: [{
        type: 'rotating-file',
        path: logConfig.path,
        period: logConfig.period,
        level: "debug"
    }],
    src: true
});


if (process.env.NODE_ENV === "test") {
    logger.level(bunyan.FATAL + 1);
}

exports.logger = logger;