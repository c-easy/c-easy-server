const ChapterDB = require('./database/chapterDB');

class Chapter {
    id;
    title;
    order;

    constructor(id, title, order) {
        this.id = id;
        this.title = title;
        this.order = order;
    }

    getMyCourses(courseList){
        let courses = [];
        courseList.forEach(course => {
            if(course.idChapter == this.id){
                courses.push(course)
            }
        });
        return courses;
    }

    static getAllChapters(){
        return ChapterDB.getAllChapters().then(function(result){
            let chapters = [];
            for (let i = 0; i < result.length; i++) {
                let chapter = Object.assign(new Chapter, result[i]);
                chapters[i] = chapter;
            }
            return chapters;
        });
    }

    async addChapter(){
        try {
            return await ChapterDB.addChapter(this);
        } catch (err) {
            throw err;
        }
    }

    async updateChapter(){
        try {
            return await ChapterDB.updateChapter(this);
        } catch (err) {
            throw err;
        }
    }

    async deleteChapter(){
        try {
            return await ChapterDB.deleteChapter(this);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = Chapter;