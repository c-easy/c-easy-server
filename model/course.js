const CourseDB = require('./database/courseDB');

class Course {
    id;
    idChapter;
    title;
    content;
    order;

    constructor(id, idChapter, title, content, order) {
        this.id = id;
        this.idChapter = idChapter;
        this.title = title;
        this.content = content;
        this.order = order;
    }

    getMyExercises(exerciseList) {
        let exercises = [];
        exerciseList.forEach(exercise => {
            if (exercise.idCourse == this.id) {
                exercises.push(exercise)
            }
        });
        return exercises;
    }

    static getAllCourses() {
        return CourseDB.getAllCourses().then(function (result) {
            let courses = [];
            for (let i = 0; i < result.length; i++) {
                let course = Object.assign(new Course, result[i]);
                courses[i] = course;
            }
            return courses;
        });
    }

    async addCourse() {
        try {
            return await CourseDB.addCourse(this);
        } catch (err) {
            throw err;
        }
    }

    async updateCourse() {
        try {
            return await CourseDB.updateCourse(this);
        } catch (err) {
            throw err;
        }
    }

    async deleteCourse() {
        try {
            return await CourseDB.deleteCourse(this);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = Course;