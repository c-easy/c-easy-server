let chapters = [{
    id: 1,
    title: "Un chapitre",
    order: 10,
}];

class ChapterDB {
    constructor() { }

    static getAllChapters() {
        return Promise.resolve(chapters);
    }

    static addChapter(chapter) {
        chapters.push(chapter);
        return Promise.resolve(chapter);
    }

    static updateChapter(chapter) {
        let index = chapters.findIndex(element => element.id == chapter.id);
        chapters[index] = chapter;
        return Promise.resolve(chapter)
    }

    static deleteChapter(chapter) {
        let oldLength = chapters.length;
        chapters.splice(chapter, 1);
        return Promise.resolve(chapters.length == oldLength - 1);
    }
}

module.exports = ChapterDB;
module.exports.chapters = chapters;