let courses = [
    {
        id: 1, idChapter: 1, title: "Un cours", content: "Un contenu", order: 10
    }];


class CourseDB {
    constructor() { }

    static getAllCourses() {
        return Promise.resolve(courses);
    }

    static addCourse(course) {
        courses.push(course);
        return Promise.resolve(course)
    }

    static updateCourse(course) {
        let index = courses.findIndex(element => element.id == course.id);
        courses[index] = course;
        return Promise.resolve(course)
    }

    static deleteCourse(course) {
        let oldLength = courses.length;
        courses.splice(course, 1);
        return Promise.resolve(courses.length == oldLength - 1)
    }
}

module.exports = CourseDB;
module.exports.courses = courses;