let currentDo = {
    idUser: 1,
    idExercise: 1,
    startTime: new Date(),
    completionTime: null,
    try: 0,
    done: 0,
    code: null
};

let dos = [
    currentDo,
    {idUser: 1, idExercise: 1000, startTime: new Date(), completionTime: null, try: 0, done: 0, code: null}
]

class DoDB {
    constructor() { }

    static getAllDosFromUser(user){
        return Promise.resolve(dos.filter(element => element.idUser == user.id));
    }

    static getDoFromUserFromExercise(user, exercice) {
        return Promise.resolve([currentDo]);
    }

    static addDo(d) {
        return Promise.resolve(d);
    }

    static updateDo(d) {
        return Promise.resolve(d);
    }
}

module.exports = DoDB;
module.exports.currentDo = currentDo;
module.exports.dos = dos;