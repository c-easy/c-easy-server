let exercises = [{
    id: 1, 
    idCourse: 1, 
    type: 1, 
    title: "Un exercice", 
    instruction: "Une instruction",
    dependency: "Des dépendances",
    main: "Un contenu", 
    correction: "Une correction", 
    order: 10,
    testSet: [{input: 1, expected: 20}]
}];

class ExerciseDB {
    constructor() { }

    static getAllExercises() {
        return Promise.resolve(exercises);
    }

    static addExercise(exercise) {
        exercises.push(exercise);
        return Promise.resolve(exercise);
    }

    static updateExercise(exercise) {
        let index = exercises.findIndex(element => element.id == exercise.id);
        exercises[index] = exercise;
        return Promise.resolve(exercise)
    }

    static deleteExercise(exercise) {
        let oldLength = exercises.length;
        exercises.splice(exercise, 1);
        return Promise.resolve(exercises.length == oldLength - 1);
    }
}

module.exports = ExerciseDB;
module.exports.exercises = exercises;