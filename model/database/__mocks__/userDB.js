let users = [
    { id: "b14009868", name: "Loïc BAYOL", type: 1, promo: 2020 },
    { id: "b17004747", name: "Marius BIDEL", type: 0, promo: 2019 },
    { id: "b17004848", name: "Léo BEAN", type: 0, promo: 2019 }
];

class UserDB {
    constructor() { }

    static getUser(user) {
        return Promise.resolve(user);
    }

    static getAllUsersFromPromo(promo) {
        let result = users.filter(user => user.promo == promo);
        return Promise.resolve(result);
    }

    static getAllPromos() {
        let promos = [];
        let result = [];
        users.forEach(user => {
            if (promos.indexOf(user.promo) == -1) {
                promos.push(user.promo);
                result.push(user);
            }
        });
        return Promise.resolve(result);
    }

    static addUser(user) {
        users.push(user);
        return Promise.resolve(user)
    }

    static deleteUser(user) {
        let oldLength = users.length;
        users.splice(user, 1);
        return Promise.resolve(users.length == oldLength - 1)
    }
}

module.exports = UserDB;
module.exports.users = users;