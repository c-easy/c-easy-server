const db = require('./connection');

class ChapterDB {
    constructor() { }

    static async getAllChapters() {
        try {
            const result = await db.pool.query("SELECT * FROM CHAPTER ORDER BY `order`");
            return result;
        } catch (err) {
            throw err;
        }
    }

    static async addChapter(chapter) {
        try {
            const result = await db.pool.query("INSERT INTO CHAPTER(`title`) VALUES(?)", [chapter.title]);
            chapter.id = result.insertId;
            chapter.order = chapter.order != undefined ? chapter.order : -1;
            return chapter;
        } catch (err) {
            throw err;
        }
    }

    static async updateChapter(chapter) {
        try {
            const result = await db.pool.query("UPDATE CHAPTER SET `order` = ?, `title`= ? WHERE `id` = ?", Object.values(chapter).reverse());
            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return chapter;
        } catch (err) {
            throw err;
        }
    }

    static async deleteChapter(chapter) {
        try {
            const result = await db.pool.query("DELETE FROM CHAPTER WHERE `id` = ?", [chapter.id]);
            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return chapter;
        } catch (err) {
            throw err;
        }
    }

}

module.exports = ChapterDB;