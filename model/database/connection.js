const mariadb = require('mariadb');
const {databaseConfig} = require('../../config/config');

module.exports = Object.freeze({
    pool: mariadb.createPool({
        host: databaseConfig.host,
        port: databaseConfig.port,
        database: databaseConfig.database,
        user: databaseConfig.user, 
        password: databaseConfig.password
   })
});