const db = require('./connection');

class CourseDB {
    constructor() { }

    static async getAllCourses() {
        try {
            const result = await db.pool.query("SELECT * FROM COURSE ORDER BY `order`");
            return result;
        } catch (err) {
            throw err;
        }
    }

    static async addCourse(course) {
        try {
            const result = await db.pool.query("INSERT INTO COURSE(`idChapter`, `title`, `content`) VALUES(?, ?, ?)", [course.idChapter, course.title, course.content]);
            course.id = result.insertId;
            course.order = course.order != undefined ? course.order : -1;
            return course;
        } catch (err) {
            if (err.errno == 1452) {
                throw new RangeError('wrongId');
            }
            throw err;
        }
    }

    static async updateCourse(course) {
        try {
            const result = await db.pool.query("UPDATE COURSE SET `order` = ?, `content` = ?, `title`= ?, `idChapter` = ? WHERE `id` = ?", Object.values(course).reverse());
            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return course;
        } catch (err) {
            if (err.errno == 1452) {
                throw new RangeError('wrongIdChapter');
            }
            throw err;
        }
    }

    static async deleteCourse(course) {
        try {
            const result = await db.pool.query("DELETE FROM COURSE WHERE `id` = ?", [course.id]);
            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return course;
        } catch (err) {
            throw err;
        }
    }

}

module.exports = CourseDB;