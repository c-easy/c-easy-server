const db = require('./connection');

class DoDB {
    constructor() { }

    static async getAllDosFromUser(user) {
        try {
            if (user.id == null) throw new Error("Invalid parameter");
            
            const result = await db.pool.query("SELECT * FROM DO WHERE `idUser` = ?", [user.id]);
            return result;
        } catch (err) {
            throw err;
        }
    }

    static async getDoFromUserFromExercise(user, exercise) {
        try {
            if (user.id == null || exercise.id == null) throw (new Error("Invalid parameter"));
            const result = await db.pool.query("SELECT * FROM DO WHERE `idUser` = ? AND `idExercise` = ?", [user.id, exercise.id]);
            return result;
        } catch (err) {
            throw err;
        }
    }

    static async addDo(d) {
        try {
            const result = await db.pool.query("INSERT INTO DO VALUES(?, ?, ?, ?, ?, ?, ?)", Object.values(d));
            d.id = result.insertId;
            return d;
        } catch (err) {
            switch (err.errno) {
                case 1452:
                    throw new RangeError('wrongId');
                case 1062:
                    throw new RangeError('duplicateDo');
            }
            throw err;
        }
    }

    static async updateDo(d) {
        try {
            const result = await db.pool.query("UPDATE DO SET `code` = ?, `done`= ?, `try`= ?, `completionTime`= ?, `startTime`= ? WHERE `idExercise` = ? AND `idUser` = ?", Object.values(d).reverse());
            if (result.affectedRows > 0) {
                return d;
            }
        } catch (err) {
            if (err.errno == 1452) {
                throw new RangeError('wrongId');
            }
            throw err;
        }
    }

}

module.exports = DoDB;