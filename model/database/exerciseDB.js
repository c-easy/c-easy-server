const db = require('./connection');
const Test = require('../test');

class ExerciseDB {
    constructor() { }

    static async getAllExercises() {
        try {
            const result = await db.pool.query("SELECT * FROM EXERCISE ORDER BY `order`");
            for (let i = 0; i < result.length; i++) {
                let resultTest = await db.pool.query("SELECT `id`, `input`, `expected` FROM TESTSET WHERE `idExercise`= ?", [result[i].id]);
                let testSet = [];
                for (let i = 0; i < resultTest.length; i++) {
                    let test = Object.assign(new Test, resultTest[i]);
                    testSet[i] = test;
                }
                result[i].testSet = testSet;
            }
            return result;
        } catch (err) {
            throw err;
        }
    }

    static async getExerciseFromId(id){
        try {
            const result = await db.pool.query("SELECT * FROM EXERCISE WHERE `id` = ?", [id]);
            if(result[0]){
                let resultTest = await db.pool.query("SELECT `id`, `input`, `expected` FROM TESTSET WHERE `idExercise`= ?", [result[0].id]);
                let testSet = [];
                for (let i = 0; i < resultTest.length; i++) {
                    let test = Object.assign(new Test, resultTest[i]);
                    testSet[i] = test;
                }
                result[0].testSet = testSet;
            }
            return result;           
        } catch (err) {
            throw err;
        }
    }

    static async addExercise(exercise) {
        try {
            const result = await db.pool.query("INSERT INTO EXERCISE(`idCourse`, `type`, `title`, `instruction`, `correction`, `dependency`, `main`) VALUES(?, ?, ?, ?, ?, ?, ?)", [exercise.idCourse, exercise.type, exercise.title, exercise.instruction, exercise.correction, exercise.dependency, exercise.main]);

            for (let i = 0; i < exercise.testSet.length; i++) {
                let test = exercise.testSet[i];
                const resultTest = await db.pool.query("INSERT INTO TESTSET(`idExercise`, `input`, `expected`) VALUES(?, ?, ?)", [result.insertId, test.input, test.expected]);
                test.id = resultTest.insertId;
            };

            exercise.order = exercise.order != undefined ? exercise.order : -1;
            exercise.id = result.insertId;
            return exercise;
        } catch (err) {
            if(err.errno == 1452){
                throw new RangeError('wrongId');
            }
            throw err;        
        }
    }

    static async updateExercise(exercise) {
        try {
            const result = await db.pool.query("UPDATE EXERCISE SET `idCourse` = ?, `type` = ?,`title` = ?, `instruction` = ?, `correction` = ?, `dependency` = ?, `main`= ?, `order` = ? WHERE `id` = ?", [exercise.idCourse, exercise.type, exercise.title, exercise.instruction, exercise.correction, exercise.dependency, exercise.main, exercise.order, exercise.id]);

            const resultTest1 = await db.pool.query("DELETE FROM TESTSET WHERE `idExercise` = ?", [exercise.id]);
            for (let i = 0; i < exercise.testSet.length; i++) {
                let test = exercise.testSet[i];
                const resultTest2 = await db.pool.query("INSERT INTO TESTSET(`idExercise`, `input`, `expected`) VALUES(?, ?, ?)", [exercise.id, test.input, test.expected]);
                test.id = resultTest2.insertId;
            };

            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return exercise;
        } catch (err) {
            if(err.errno == 1452){
                throw new RangeError('wrongIdCourse');
            }
            throw err;
        }
    }

    static async deleteExercise(exercise) {
        try {
            const result = await db.pool.query("DELETE FROM EXERCISE WHERE `id` = ?", [exercise.id]);
            if (result.affectedRows == 0) {
                throw new RangeError('wrongId');
            }
            return exercise;
        } catch (err) {
            throw err;
        }
    }

}

module.exports = ExerciseDB;