const db = require('./connection');

class UserDB {
  constructor(){}

  static async getUser(user) {
    try {
      if(user.id == null) throw new Error("Invalid parameter");
      const result = await db.pool.query("SELECT * FROM USER WHERE `id` = ?",[user.id]);
      return result[0];
    } catch (err) {
      throw err;
    }
  }

  static async getAllUsersFromPromo(promo) {
    try {
      const result = await db.pool.query("SELECT * FROM USER WHERE `promo` = ?",[promo]);
      return result;
    } catch (err) {
      throw err;
    }
  }

  static async getAllPromos() {
    try {
      const result = await db.pool.query("SELECT DISTINCT `promo` FROM USER ORDER BY `promo`");
      return result;
    } catch (err) {
      throw err;
    }
  }

  static async addUser(user) {
    try {
      const result = await db.pool.query("INSERT INTO USER VALUES(?, ?, ?, ?, ?)", Object.values(user));
      return user;
    } catch (err) {
        switch (err.errno) {
            case 1062:
                throw new RangeError('duplicateUser');
            case 1406:
                throw new RangeError('tooLongId');
        }
        throw err;
    }
  }

    static async updateUser(user) {
      try {
          const result = await db.pool.query("UPDATE USER SET `password` = ? WHERE `id` = ?", [user.password, user.id]);
          if (result.affectedRows == 0) {
              throw new RangeError('wrongId');
          }
          return user;
      } catch (err) {
          throw err;
      }
  }

  static async deleteUser(user) {
    try {
      const result = await db.pool.query("DELETE FROM USER WHERE `id` = ?", [user.id]);
      if (result.affectedRows == 0) {
        throw new RangeError('wrongId');
      }
      return user;
    } catch (err) {
      throw err;
    }
  }
  
  static async deletePromo(promo){
    try {
      const result = await db.pool.query("DELETE FROM USER WHERE `promo` = ?", [promo]);
      if (result.affectedRows == 0) {
        throw new RangeError('wrongPromo');
      }
      return promo;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = UserDB;