const DoDB = require('./database/doDB');
const User = require('./user');

class Do {
    idUser;
    idExercise;
    startTime;
    completionTime;
    try;
    done;
    code;

    constructor(idUser, idExercise, startTime, completionTime, tr, done, code) {
        this.idUser = idUser;
        this.idExercise = idExercise;
        this.startTime = startTime;
        this.completionTime = completionTime;
        this.try = tr;
        this.done = done;
        this.code = code;
    }

    static async getAllDosFromUser(user) {
        try {
            user = await User.getUser(user);
            if (user) {
                return DoDB.getAllDosFromUser(user).then(function (result) {
                    let dos = [];
                    for (let i = 0; i < result.length; i++) {
                        let d = Object.assign(new Do, result[i]);
                        dos[i] = d;
                    }
                    return dos;
                }).catch(function (err) {
                    throw err;
                });
            } else {
                throw new RangeError('wrongId');
            }
        } catch (err) {
            throw err;
        }
    }

    static async getAllDosFromPromo(promo) {
        try {
            let users = await User.getAllUsersFromPromo(promo);
            let dos = [];

            for (let i = 0; i < users.length; i++) {
                let d = await Do.getAllDosFromUser(users[i]);
                if (d.length > 0) dos.push(d);
            }

            return dos;
        } catch (err) {
            throw err;
        }
    }

    static async getDoFromUserFromExercise(user, exercise) {
        try {
            user = await User.getUser(user);
            if (user) {
                return DoDB.getDoFromUserFromExercise(user, exercise).then(function (result) {
                    let d = null;
                    if (result.length > 0) {
                        d = Object.assign(new Do, result[0]);
                    }
                    return d;
                });
            } else {
                throw new RangeError('wrongId');
            }
        } catch (err) {
            throw err;
        }
    }

    async addDo() {
        try {
            return await DoDB.addDo(this);
        } catch (err) {
            throw err;
        }
    }

    async updateDo() {
        try {
            return await DoDB.updateDo(this);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = Do;