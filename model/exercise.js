const ExerciseDB = require('./database/exerciseDB');

class Exercise {
    id;
    idCourse;
    type;
    title;
    instruction;
    correction;
    dependency;
    main;
    order;
    testSet = [];

    constructor(id, idCourse, type, title, instruction, correction, dependency, main, order, testSet) {
        this.id = id;
        this.idCourse = idCourse;
        this.type = type;
        this.title = title;
        this.instruction = instruction;
        this.correction = correction;
        this.dependency = dependency;
        this.main = main;
        this.order = order;
        this.testSet = testSet;
    }

    static getAllExercises() {
        return ExerciseDB.getAllExercises().then(function (result) {
            let exercises = [];
            for (let i = 0; i < result.length; i++) {
                let exercise = Object.assign(new Exercise, result[i]);
                exercises[i] = exercise;
            }
            return exercises;
        });
    }

    static getExerciseFromId(id){
        return ExerciseDB.getExerciseFromId(id).then(function(result){
            let exercise = null;
            if(result.length > 0) {
                exercise = Object.assign(new Exercise, result[0]);
            }
            return exercise;
        });
    }

    async addExercise() {
        try {
            return await ExerciseDB.addExercise(this);
        } catch (err){
            throw err;
        }
    }

    async updateExercise() {
        try {
            return await ExerciseDB.updateExercise(this);
        } catch (err){
            throw err;
        }
    }

    async deleteExercise() {
        try {
            return await ExerciseDB.deleteExercise(this);
        } catch (err){
            throw err;
        }
    }
}

module.exports = Exercise;