class Test {
    id;
    input;
    expected;

    constructor(id, input, expected) {
        this.id = id;
        this.input = input;
        this.expected = expected;
    }
}

module.exports = Test;