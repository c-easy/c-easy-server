const UserDB = require('./database/userDB');

class User {
    id;
    name;
    password;
    type;
    promo;

    constructor(id, name, password, type, promo) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.type = type;
        this.promo = promo;
    }

    static async getUser(user){
        try {
            return await UserDB.getUser(user);
        } catch (err) {
            throw err;
        }
    }

    static getAllUsersFromPromo(promo){
        return UserDB.getAllUsersFromPromo(promo).then(function(result){
            let users = [];
            for (let i = 0; i < result.length; i++) {
                let user = Object.assign(new User, result[i]);
                users[i] = user;
            }
            return users;
        }).catch( function (err) {
            throw err;
        });
    }

    static getAllPromos(){
        return UserDB.getAllPromos().then(function(result){
            let promos = [];
            for (let i = 0; i < result.length; i++) {
                promos[i] = result[i].promo;
            }
            return promos;
        }).catch( function (err) {
            throw err;
        });
    }

    static async deletePromo(promo){
        try {
            return await UserDB.deletePromo(promo);
        } catch (err) {
            throw err;
        }
    }

    async addUser(){
        try {
            return await UserDB.addUser(this);
        } catch (err) {
            throw err;
        }
    }

    async updateUser(){
        try {
            return await UserDB.updateUser(this);
        } catch (err) {
            throw err;
        }
    }

    async deleteUser(){
        try {
            return await UserDB.deleteUser(this);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = User;