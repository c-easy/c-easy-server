const express = require('express'),
    Chapter = require('../model/chapter');
Course = require('../model/course');
Exercise = require('../model/exercise');
var router = express.Router();

// /api/chapter/

router.get('/', async function (request, response) {
    let chapters = await Chapter.getAllChapters();
    let coursesList = await Course.getAllCourses();
    let exercicesList = await Exercise.getAllExercises();
    for (var chapter of chapters) {
        chapter.courses = chapter.getMyCourses(coursesList);
        for (var course of chapter.courses) {
            course.exercises = course.getMyExercises(exercicesList);
        }
    }
    let result = {
        success: true,
        chapters: chapters
    };
    response.status(200).json(result);
});

router.post('/add', async function (request, response) {
    let chapter = request.body.chapter;
    if (chapter && chapter instanceof Object) {
        if (!chapter.title || chapter.order < -1) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            chapter = Object.assign(new Chapter(), chapter);
            var result = await chapter.addChapter();
            if (result == chapter) {
                result = {
                    success: true,
                    message: "Chapter added to the database",
                    chapter: result
                };
                response.status(200).json(result);
            } else {
                result = {
                    success: false,
                    error: result
                };
                response.status(500).json(result);
            }
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.put('/update', async function (request, response) {
    let chapter = request.body.chapter;
    if (chapter && chapter instanceof Object) {
        if (!chapter.title || !chapter.id || !chapter.order || chapter.order < -1) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            chapter = Object.assign(new Chapter(), chapter);
            chapter.updateChapter().then(res => {
                let result = {
                    success: true,
                    message: "Chapter updated",
                    chapter: res
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err,
                };
                if (err instanceof RangeError) {
                    switch (err.message) {
                        case "wrongId":
                            result.error = "There is no chapter with this ID."
                            response.status(422).json(result);
                            break;
                        default:
                            result.error = err.message;
                            response.status(400).json(result);
                    }
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.delete('/delete', async function (request, response) {
    let chapter = request.body.chapter;
    if (chapter && chapter instanceof Object) {
        if (!chapter.id) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            chapter = Object.assign(new Chapter(), chapter);
            chapter.deleteChapter().then(res => {
                let result = {
                    success: true,
                    message: "Chapter deleted",
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err,
                };
                if (err instanceof RangeError) {
                    result.error = "There is no chapter with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});
module.exports = router;