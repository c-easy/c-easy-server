const express = require('express'),
      {compileAndTest} = require("../compiler/run-test"),
      Do = require('../model/do'),
      Exercise = require('../model/exercise');
var router = express.Router();

// /api/compile/
router.post('/compileAndTest', async function(request, response){
    let source = request.body.source;
    let exercise = request.body.exercise;
    let user = request.body.token;
    if(source && exercise && exercise instanceof Object){
        if(source.indexOf("#include") != -1){
            let result = {
                success: false,
                error: "Declaring new dependencies is prohibited."
            }
            response.status(400).send(result);
        }
        try {
            exercise = await Exercise.getExerciseFromId(exercise.id);
            if(exercise != null){
                let d = await Do.getDoFromUserFromExercise(user, exercise);
                // If the "Do" is null then we have to create a new one
                if(!d){
                    d = new Do(user.id, exercise.id, new Date(), null, 0, 0, null);
                    d = d.addDo();
                }
                let code = exercise.dependency + "\n";
                code += source + "\n";
                code += exercise.main;
                compileAndTest(code, exercise.testSet).then(async function(res){
                    let result = {};
                    // If there is no testSet then the compilation failed and we have to return the compilation errors
                    let pass = false;
                    if(!res.testSet){
                        result = {
                            success: true,
                            compiled: false,
                            error: res.stderr
                        }
                    } else {
                        pass = true;
                        // For each test, we check if the output equals the expected
                        res.testSet.forEach(function(test){
                            pass *= (test.expected == test.output);
                        });
                        result = {
                            success: true,
                            compiled: true,
                            pass: Boolean(pass),
                            testSet: res.testSet
                        }
                    }
    
                    // If the user did not already complete the exercise, we have to increment the number of try and then if he has completed it, 
                    // update the object adequately.
                    if(!d.done){
                        d.try++;
                        if(pass){
                            d.done = pass;
                            d.completionTime = new Date();
                        }
                        d.code = source;
                        d = await d.updateDo();
                    }
                    result.do = d;
                    response.status(200).send(result);
                }).catch(function(error){
                    let result = {
                        success: false,
                        error: error.message
                    }
                    response.status(500).send(result);
                });
            } else {
                let result = {
                    success: false,
                    error: "There is no exercise with this ID"
                }
                response.status(422).send(result);
            }
        } catch (err) {
            let result = {
                success: false,
                error: err.message
            }
            response.status(500).send(result);            
        }

    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);    
    }
});

router.post('/runTest', async function(request, response){
    let exercise = request.body.exercise;
    if(exercise && exercise instanceof Object){
        if(exercise.correction && exercise.dependency && exercise.main && exercise.testSet){
            let code = exercise.dependency + "\n";
            code += exercise.correction + "\n";
            code += exercise.main;
            compileAndTest(code, exercise.testSet).then(function(res){
                let result = {};
                // If there is no testSet then the compilation failed and we have to return the compilation errors
                if(!res.testSet){
                    result = {
                        success: true,
                        compiled: false,
                        error: res.stderr
                    }
                } else {
                    result = {
                        success: true,
                        compiled: true,
                        testSet: res.testSet
                    }
                }
                response.status(200).send(result);
            }).catch(function(error){
                let result = {
                    success: false,
                    error: error.message
                }
                response.status(500).send(result);
            });
        } else {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

module.exports = router;