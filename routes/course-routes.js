const express = require('express'),
      multer = require('multer'),
      fs = require('fs'),
      Course = require('../model/course'),
      Exercise = require('../model/exercise');
var router = express.Router();

// Configuration for the PDF upload
const fileFilter = function(request, file, callback){
    const allowedTypes =  ['application/pdf'];
    
    if(!allowedTypes.includes(file.mimetype)){
        const error = new TypeError('Invalid File Type');
        return callback(error, false);
    }

    callback(null, true);
};

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        let filename = file.originalname.replace(".pdf","");
        cb(null, filename + "-" + Date.now() +'.pdf');
    }
 });

const upload = multer({
    storage: storage,
    fileFilter
}).single('pdffile');

// /api/course/
router.get('/', async function (request, response) {
    let courses = await Course.getAllCourses();
    let exercicesList = await Exercise.getAllExercises();
    for (var course of courses) {
        course.exercises = course.getMyExercises(exercicesList);
    }
    let result = {
        success: true,
        courses: courses
    };
    response.status(200).json(result);
});

router.post('/add', async function (request, response) {
    let course = request.body.course;
    if (course && course instanceof Object) {
        if (!course.title || !course.idChapter || !course.content || course.order < -1) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            course = Object.assign(new Course(), course);
            course.addCourse().then(res => {
                let result = {
                    success: true,
                    message: "Course added to the database",
                    course: res
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err
                };
                if (err instanceof RangeError) {
                    result.error = "There is no chapter with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.put('/update', async function (request, response) {
    let course = request.body.course;
    if (course && course instanceof Object) {
        if (!course.title || !course.id || !course.order || !course.idChapter || !course.content || course.order < -1) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            if(course.newContent){
                let filename = process.env.PWD + "/" + course.content.substring(course.content.lastIndexOf('/') - 7);
                fs.unlink(filename, function(error){
                    if(error){
                        console.log(error);
                    }
                });
            }
            course.content = course.newContent ? course.newContent : course.content;
            delete course.newContent;
            course = Object.assign(new Course(), course);
            course.updateCourse().then(res => {
                let result = {
                    success: true,
                    message: "Course updated",
                    course: res
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err,
                };
                if (err instanceof RangeError) {
                    switch (err.message) {
                        case "wrongIdChapter":
                            result.error = "There is no chapter with this ID."
                            response.status(422).json(result);
                            break;
                        case "wrongId":
                            result.error = "There is no course with this ID.";
                            response.status(422).json(result);
                            break;
                        default:
                            result.error = err.message;
                            response.status(400).json(result);
                    }
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.delete('/delete', async function (request, response) {
    let course = request.body.course;
    if (course && course instanceof Object) {
        if (!course.id && !course.content) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            let filename = process.env.PWD + "/" + course.content.substring(course.content.lastIndexOf('/') - 7);
            fs.unlink(filename, function(error){
                if(error){
                    console.log(error);
                }
            });
            course = Object.assign(new Course(), course);
            course.deleteCourse().then(res => {
                let result = {
                    success: true,
                    message: "Course deleted",
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err,
                };
                if (err instanceof RangeError) {
                    result.error = "There is no course with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/upload', async function (request, response){
    upload(request, response, function(err){
        if(err instanceof multer.MulterError){
            response.status(420).json(err);
        } else if (err){
            response.status(400).json(err);
        }
        response.status(200).json({
            success: true,
            file: request.file.path
        });

    });
});

module.exports = router;