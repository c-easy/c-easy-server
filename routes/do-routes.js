const express = require('express'),
      Do = require('../model/do');
var router = express.Router();

// /api/do/

router.post('/getAllDos', function (request, response) {
    let user = request.body.token;
    Do.getAllDosFromUser(user).then(res => {
        let result = {
            success: true,
            dos: res
        }
        response.status(200).json(result);
    }).catch(error => {
        let result = {
            success: false,
            error: error.message
        }
        if(error instanceof RangeError){
            result.error = "There is no user with this ID."
            response.status(422).json(result);
        } else {
            response.status(500).json(result);
        }
    });
});

router.post('/getAllDosFromUser', function (request, response) {
    let user = request.body.user;
    if(user && user instanceof Object){
        Do.getAllDosFromUser(user).then(res => {
            let result = {
                success: true,
                dos: res
            }
            response.status(200).json(result);
        }).catch(error => {
            let result = {
                success: false,
                error: error.message
            }
            if(error instanceof RangeError){
                result.error = "There is no user with this ID."
                response.status(422).json(result);
            } else {
                response.status(500).json(result);
            }
        });
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/getAllDosFromPromo', function (request, response) {
    let promo = Number(request.body.promo);
    if(promo){
        Do.getAllDosFromPromo(promo).then(res => {
            let result = {
                success: true,
                dos: res
            }
            response.status(200).json(result);
        }).catch(error => {
            let result = {
                success: false,
                error: error.message
            }
            response.status(500).json(result);
        });
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/getDoFromExercise', async function (request, response) {
    let exercise = request.body.exercise;
    let user = request.body.token;
    if (exercise && user && exercise instanceof Object && user instanceof Object) {
        if(!exercise.id && !user.id){
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            try {
                let d = await Do.getDoFromUserFromExercise(user, exercise);
                let result = {
                    success: true,
                    do: d
                };
                response.status(200).json(result);
            } catch (error) {
                let result = {
                    success: false,
                    error: error.message
                }
                if(error instanceof RangeError){
                    result.error = "There is no user with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            }
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/getDoFromUserFromExercise', async function (request, response) {
    let exercise = request.body.exercise;
    let user = request.body.user;
    if (exercise && user && exercise instanceof Object && user instanceof Object) {
        if(!exercise.id && !user.id){
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            try {
                let d = await Do.getDoFromUserFromExercise(user, exercise);
                let result = {
                    success: true,
                    do: d
                };
                response.status(200).json(result);
            } catch (error) {
                let result = {
                    success: false,
                    error: error.message
                }
                if(error instanceof RangeError){
                    result.error = "There is no user with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            }
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/add', async function (request, response) {
    let exercise = request.body.exercise;
    let user = request.body.token;
    if (exercise && user && exercise instanceof Object && user instanceof Object) {
        if(!exercise.id && !user.id){
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            let d = new Do(user.id, exercise.id, new Date(), null, 0, 0, null);
            let result = {};
            d.addDo().then(result => {
                result = {
                    success: true,
                    message: "Do added",
                    do: result
                };
                response.status(200).json(result);
            }).catch(err => {
                result.success = false;
                if(err instanceof RangeError){
                    switch (err.message) {
                        case "wrongId":
                            result.message = "There is no exercise with this ID.";
                            response.status(422).json(result);
                            break;
                        case "duplicateDo":
                            result.message = "There is already a Do with those IDs.";
                            response.status(423).json(result);
                            break;
                        default:
                            result.message = err.message;
                            response.status(500).json(result);
                    }
                } else {
                    result.error = err;
                    response.status(400).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

module.exports = router;