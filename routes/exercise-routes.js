const express = require('express');
var router = express.Router();
const Exercise = require('../model/exercise');

// /api/exercise/

router.get('/', async function (request, response) {
    let exercises = await Exercise.getAllExercises();
    let result = {
        success: true,
        exercises: exercises
    };
    response.status(200).json(result);
});

router.post('/add', async function (request, response) {
    let exercise = request.body.exercise;
    if (exercise && exercise instanceof Object) {
        if (!exercise.title || !exercise.type || !exercise.idCourse || !exercise.instruction || !exercise.correction || !exercise.main || !exercise.dependency || !exercise.testSet) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            try {
                exercise = Object.assign(new Exercise(), exercise);
                var result = await exercise.addExercise();
                if (result == exercise) {
                    result = {
                        success: true,
                        message: "Exercise added to the database",
                        exercise: result
                    };
                    response.status(200).json(result);
                } else {
                    result = {
                        success: false,
                        error: result
                    };
                    response.status(500).json(result);
                }
            } catch (err) {
                result = {
                    success: false,
                    error: err
                };
                if (err instanceof RangeError) {
                    result.error = "There is no course with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            }

        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.put('/update', async function (request, response) {
    let exercise = request.body.exercise;
    if (exercise && exercise instanceof Object) {
        if (!exercise.id || !exercise.type || !exercise.title || !exercise.idCourse || !exercise.instruction || !exercise.correction || !exercise.main || !exercise.dependency || !exercise.order) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            exercise = Object.assign(new Exercise(), exercise);
            exercise.updateExercise().then(res => {
                let result = {
                    success: true,
                    message: "Exercise updated.",
                    exercise: res
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err
                };
                if (err instanceof RangeError) {
                    switch (err.message) {
                        case "wrongIdCourse":
                            result.error = "There is no course with this ID."
                            response.status(422).json(result);
                            break;
                        case "wrongId":
                            result.error = "There is no exercise with this ID.";
                            response.status(422).json(result);
                            break;
                        default:
                            result.error = err.message;
                            response.status(500).json(result);
                    }
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.delete('/delete', async function (request, response) {
    let exercise = request.body.exercise;
    if (exercise && exercise instanceof Object) {
        if (!exercise.id) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            exercise = Object.assign(new Exercise(), exercise);
            exercise.deleteExercise().then(res => {
                let result = {
                    success: true,
                    message: "Exercise deleted",
                };
                response.status(200).json(result);
            }).catch(err => {
                let result = {
                    success: false,
                    error: err,
                };
                if (err instanceof RangeError) {
                    result.error = "There is no exercise with this ID."
                    response.status(422).json(result);
                } else {
                    response.status(500).json(result);
                }
            });
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});


module.exports = router;