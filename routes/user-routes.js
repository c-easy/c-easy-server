const express = require('express'),
      argon2 = require('argon2'),
      crypto = require('crypto'),
      Token = require('../server/token'),
      User = require('../model/user');
var router = express.Router();

// /api/user/
router.post('/login', async function(request, response){
    let user = {
        id: request.body.id,
        password: request.body.password
    }
    try {
        user = await User.getUser(user);
        if(user){
            let verifPassword = await argon2.verify(user.password, request.body.password);
            if(verifPassword){
                delete user.password;
                let result = {
                    success: true,
                    user: user,
                    token: Token.createToken(user.id, user.name, user.type)
                }
                response.status(200).json(result);
            } else {
                let result = {
                    success: false,
                    message: "Wrong password."
                };
                response.status(400).json(result);                
            }
        } else {
            let result = {
                success: false,
                message: "No user with this ID."
            };
            response.status(400).json(result);
        }
    } catch (error) {
        console.log(error)
        let result = {
            success: false,
            message: error.message
        };
        response.status(400).json(result);
    }
});

router.post('/create', async function (request, response){
    let newUser = request.body.newUser;
    if(newUser && newUser instanceof Object){
        if(newUser.id && newUser.name && newUser.promo){
            let result = {};
            try {
                let user = {};
                if(newUser.type == 1){
                    let password = await argon2.hash(newUser.password);
                    newUser.password = password;
                    user = Object.assign(new User(), newUser);
                    user = await user.addUser();
                    delete user.password;
                } else {
                    let randomPassword = crypto.randomBytes(3).toString('hex');
                    let password = await argon2.hash(randomPassword);
                    newUser.password = password;
                    user = Object.assign(new User(), newUser);
                    user = await user.addUser();
                    user.password = randomPassword;
                }
                result = {
                    success: true,
                    user: user
                }
                response.status(200).json(result);
            } catch(err) {
                result.success = false;
                if(err instanceof RangeError){
                    switch (err.message) {
                        case "duplicateUser":
                            result.message = "There is already a User with this ID.";
                            response.status(423).json(result);
                            break;
                        case "tooLongId":
                            result.message = "Your ID is too long for my database.";
                            response.status(422).json(result);
                            break;
                        default:
                            result.message = err.message;
                            response.status(500).json(result);
                    }
                } else {
                    result.error = err;
                    response.status(500).json(result);
                }              
            }
        } else {
            let result = {
                success: false,
                message: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        }
    } else  {
        let result = {
            success: false,
            message: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.put('/update', async function (request, response){
    let oldUser = request.body.oldUser;
    if(oldUser && oldUser instanceof Object){
        if(oldUser.id){
            let result = {};
            try {
                let user = {};
                if(oldUser.type == 1){
                    let password = await argon2.hash(oldUser.password);
                    oldUser.password = password;
                    user = Object.assign(new User(), oldUser);
                    user = await user.updateUser();
                    delete user.password;
                } else {
                    let randomPassword = crypto.randomBytes(3).toString('hex');
                    let password = await argon2.hash(randomPassword);
                    oldUser.password = password;
                    user = Object.assign(new User(), oldUser);
                    user = await user.updateUser();
                    user.password = randomPassword;
                }
                result = {
                    success: true,
                    user: user
                }
                response.status(200).json(result);
            } catch(err) {
                result.success = false;
                if(err instanceof RangeError){
                    switch (err.message) {
                        case "wrongId":
                            result.message = "There is no user with this ID.";
                            response.status(423).json(result);
                            break;
                        default:
                            result.message = err.message;
                            response.status(400).json(result);
                    }
                } else {
                    result.error = err.message;
                    response.status(400).json(result);
                }              
            }
        } else {
            let result = {
                success: false,
                message: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        }
    } else  {
        let result = {
            success: false,
            message: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.delete('/delete', async function (request, response){
    let oldUser = request.body.oldUser;
    let result = {};
    if(oldUser && oldUser instanceof Object){
        if(oldUser.id){
            if(oldUser.id == request.body.token.id){
                result = {
                    success: false,
                    message: "You cannot delete yourself."
                }
                response.status(400).json(result);
            } else {
                try {
                    let user = {};
                    user = Object.assign(new User(), oldUser);
                    user = await user.deleteUser();
                    result = {
                        success: true,
                        message: "User deleted."
                    }
                    response.status(200).json(result);
                } catch(err) {
                    result.success = false;
                    if(err instanceof RangeError){
                        switch (err.message) {
                            case "wrongId":
                                result.message = "There is no user with this ID.";
                                response.status(423).json(result);
                                break;
                            default:
                                result.message = err.message;
                                response.status(400).json(result);
                        }
                    } else {
                        result.error = err.message;
                        response.status(400).json(result);
                    }              
                }
            }
        } else {
            result = {
                success: false,
                message: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        }
    } else  {
        result = {
            success: false,
            message: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.delete('/deletePromo', async function (request, response){
    let promo = Number(request.body.promo);
    if(promo){
        try {
            let promos = await User.deletePromo(request.body.promo);
            let result = {
                success: true,
                message: "Promo deleted."
            };
            response.status(200).json(result);
        } catch (err) {
            let result = {
                success: false,
                message: err.message
            }
            if(err instanceof RangeError){
                result.message = `There is no promotion ${promo}.`
            }
            response.status(400).json(result);
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.get('/getAllPromos', async function (request, response) {
    try {
        let promos = await User.getAllPromos();
        let result = {
            success: true,
            promos: promos
        };
        response.status(200).json(result);
    } catch (err) {
        let result = {
            success: false,
            message: err.message
        }
        response.status(400).json(result);
    }
});

router.post('/getAllUsersFromPromo', async function (request, response) {
    let promo = Number(request.body.promo);
    if (promo) {
        try {
            let users = await User.getAllUsersFromPromo(promo);
            result = {
                success: true,
                users: users
            };
            response.status(200).json(result);
        } catch (err){
            let result = {
                success: false,
                message: err.message
            }
            response.status(400).json(result);
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

router.post('/getUser', async function (request, response) {
    let user = request.body.user;
    if (user && user instanceof Object) {
        if (!user.id) {
            let result = {
                success: false,
                error: "Invalid syntax, please read the doc"
            };
            response.status(400).json(result);
        } else {
            try {
                user = await User.getUser(user);
                if(user){
                    delete user.password;
                    let result = {
                        success: true,
                        user: user
                    };

                    response.status(200).json(result);                
                } else {
                    let result = {
                        success: false,
                        message: "No user with this ID."
                    };
                    response.status(422).json(result);
                }    
            } catch (error) {
                let result = {
                    success: false,
                    message: error
                };
                response.status(500).json(result);                
            }
        }
    } else {
        let result = {
            success: false,
            error: "Invalid syntax, please read the doc"
        };
        response.status(400).json(result);
    }
});

module.exports = router;