const express = require('express');

var router = express.Router();

const Token = require('../server/token');

router.use(function (request, response, next) {
    let token = request.headers.authorization;
    // Routes which do not need any authentication.
    const noAuthRoutes = ["/user/login"];
    // Available routes for non-admin users.
    const userRoutes = ["/chapter", "/compile/compileAndTest", "/do/add", "/do/getDoFromExercise", "/do/getAllDos", "/exercise"];
    if(!noAuthRoutes.includes(request.path) && request.method != "OPTIONS"){
        if(token && token.startsWith("Bearer ")){
            token = token.slice(7, token.length);
            Token.checkValidity(token).then(function(res){
                request.body.token = res;
                if(!userRoutes.includes(request.path) && res.type != 1){
                    let result = {
                        success: false,
                        error: "Only administrators can access this route."
                    };
                    return response.status(403).send(result);
                }
                next();
            }).catch(function(error){
                let result = {
                    success: false,
                    error: "Token invalid."
                };
                return response.status(403).send(result);
            });
        } else {
            let result = {
                success: false,
                error: "We need a token to pursue, please login or provide a valid authorization token."
            };
            return response.status(401).send(result);
        }
    } else {
        next();
    }

});

router.use('/user', require('../routes/user-routes'));
router.use('/compile', require('../routes/compile-routes'));
router.use('/course', require('../routes/course-routes'));
router.use('/exercise', require('../routes/exercise-routes'));
router.use('/chapter', require('../routes/chapter-routes'));
router.use('/do', require('../routes/do-routes'));

module.exports = router;