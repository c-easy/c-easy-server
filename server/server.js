const express = require('express'),
      helmet  = require('helmet'),
      https   = require('https'),
      express_enforces_ssl = require('express-enforces-ssl'),
      fs      = require('fs');

const routes = require('./routes');
const {serverConfig} = require('../config/config');

const app = express();

app.set('port', process.env.PORT || serverConfig.port);
app.use(helmet({
    frameguard: false
}));
app.use(express.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with, Content-Type, Authorization, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(express_enforces_ssl());
app.use('/uploads', express.static('./uploads'));
app.use('/api', routes);

module.exports = {
    /**
     * Starts the server with the host and port in arguments.
     * @param {String} host 
     * @param {Number} port 
     */
    startServer: function(host, port){
        app.set('port', process.env.PORT || port);
        app.set('host', process.env.HOST || host);

        let options = {
            key: fs.readFileSync(serverConfig.key),
            cert: fs.readFileSync(serverConfig.cert)
        };

        https.createServer(options, app).listen(app.get('port'), app.get('host'), function() {
            console.log('Express server listening on port ' + app.get('port'));
        });
    }
}
