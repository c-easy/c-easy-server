const jwt = require('jsonwebtoken');

const {serverConfig} = require('../config/config');

module.exports = {
    /**
     * Creates a JWT token based on the id, the name and the type of an user. 
     * @param {String} id
     * @param {String} name 
     * @param {Number} type 
     */
    createToken: function(id, name, type){
        let payload = {
            id: id,
            name: name,
            type: type
        };

        return jwt.sign(payload, serverConfig.token.secret, {expiresIn: serverConfig.token.expiresIn});
    },

    /**
     * Checks the valadity of the token and returns its content if valid.
     * @param {String} token 
     */
    checkValidity: function(token){
        return new Promise(function(resolve, reject){
            jwt.verify(token, serverConfig.token.secret, function(err, decoded){
                if (err){
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
    }
}
