const {compileC} = require("../../compiler/compile");
const write = require("../../compiler/write");
const crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const random = crypto.randomBytes(20);
const filepath = path.join(write.tmpFolder, random.toString('hex') + ".c");
const executablePath = path.join(write.tmpFolder, random.toString('hex') + ".out");

beforeEach(() => {
    jest.spyOn(crypto, 'randomBytes').mockReturnValue(random);
});

afterEach(() => {
    crypto.randomBytes.mockRestore();
    fs.unlink(filepath, function(err){
    });
    fs.unlink(executablePath, function(err){
    });
});

test("Should compile and then return the executable path", () => {
    write.writeSource("int main(int argc, char *argv[]){return 0;}");
    return compileC(filepath).then(data => {
        expect(data).toBe(executablePath);
    });
});

test("Should not compile and return compilation errors", () => {
    write.writeSource("int main(int argc, char *argv[]){return 0}");
    return compileC(filepath).then(data => {
        expect(data.code).toBe(1);
        expect(data.stderr).toContain("error: expected");
    });
});