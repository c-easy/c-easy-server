const {execute} = require("../../compiler/execute");

jest.useFakeTimers();

test('Should kill the command if the execution exceed the TIMEOUT', () => {
    execute("echo", ["Ceci est un test"], data => {
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), execute.TIMEOUT);
        expect(data.stderr).toBe("I killed the sheriff");
    });
});

test('Should execute a command and return the output', () => {
    return execute("echo", ["Ceci est un test"]).then(data => {
        expect(data.stdout).toBe("Ceci est un test\n");
    });
});
