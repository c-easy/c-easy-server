const {compileAndTest} = require('../../compiler/run-test');
const squareSource = '#include <stdio.h>\n#include <stdlib.h>\nint square(int a){return a*a;}int main(int argc, char *argv[]){printf("%d", square(atoi(argv[1])));return 0;}';

test("Should return an error saying that we need an array of test", () => {
    return compileAndTest(squareSource, null).catch(error => {
        expect(error).toStrictEqual(new Error("testSet should be an array of Test"));
    });
});

test("Should return an error asking to use the correct syntax for a test", () => {
    let testSet = [{"anput": 2}];
    return compileAndTest(squareSource, testSet).catch(error => {
        expect(error).toStrictEqual(new Error(JSON.stringify(testSet[0]) + " has no input, please use the correct syntax"));
    });
});

test("Should return an error saying that tests must be Objects", () => {
    let testSet = [2];
    return compileAndTest(squareSource, testSet).catch(error => {
        expect(error).toStrictEqual(new Error(testSet[0] + " is not an object, please use the correct syntax"));
    });
});

test("Should compile and execute the code then return the outputs", () => {
    let testSet = [{input: 1, expected: 1}, {input: 10, expected: 100}];
    return compileAndTest(squareSource, testSet).then(data => {
        expect(data.testSet[0].output).toBe(testSet[0].expected.toString());
        expect(data.testSet[1].output).toBe(testSet[1].expected.toString());
    });
});

test("Should not compile and say why", () => {
    let testSet = [{input: 1, expected: 1}, {input: 10, expected: 100}];
    return compileAndTest(squareSource.slice(10, squareSource.length), testSet).then(data => {
        expect(data.code).toBe(1);
        expect(data.stderr).toContain("error:");;
    });
});
