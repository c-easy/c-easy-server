const write = require("../../compiler/write");
const crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const random = crypto.randomBytes(20);
const filepath = path.join(write.tmpFolder, random.toString('hex') + ".c");

beforeEach(() => {
    jest.spyOn(crypto, 'randomBytes').mockReturnValue(random);
});

afterEach(() => {
    crypto.randomBytes.mockRestore();
    fs.unlink(filepath, function(err){
        return err;
    });
});

test('Write a file with a random name and return the filepath', () => {
    return write.writeSource("Ceci est un essai").then(data => {
        expect(data).toBe(filepath);
    });
});