const Chapter = require('../../model/chapter');
const ChapterDB = require('../../model/database/chapterDB');
jest.mock('../../model/database/chapterDB');
let abstractCourse = { 
    id: 1, 
    idChapter: 1
};

let newChapter = new Chapter(2, "Deux chapitres", 2);

beforeEach(() => {
});

test('Should call ChapterDB.getAllChapter and return a list of Chapter', () => {
    const mockGet = jest.spyOn(ChapterDB, 'getAllChapters');
    
    return Chapter.getAllChapters().then(data => {
        expect(mockGet).toHaveBeenCalled();
        expect(data).toStrictEqual([Object.assign(new Chapter(), ChapterDB.chapters[0])]);
    })
});

test('Should add all the courses of the chapter to it', () =>{
    let chapter = Object.assign(new Chapter(), ChapterDB.chapters[0]);
    chapter.courses = chapter.getMyCourses([abstractCourse]);
    
    expect(chapter.courses).toContain(abstractCourse);
});

test('Should call ChapterDB.addChapter and add a chapter', () => {
    const mock = jest.spyOn(ChapterDB, 'addChapter');

    return newChapter.addChapter().then(data => {
        expect(ChapterDB.addChapter).toHaveBeenCalled();
        expect(data).toBe(newChapter);
        expect(ChapterDB.chapters).toContain(newChapter);
    });
});

test('Should call ChapterDB.updateChapter and update a chapter', () => {
    const mock = jest.spyOn(ChapterDB, 'updateChapter');

    newChapter.title = "Troisième chapitre";
    return newChapter.updateChapter().then(data => {
        expect(ChapterDB.updateChapter).toHaveBeenCalled();
        expect(data).toBe(newChapter);
        expect(ChapterDB.chapters).toContain(newChapter);
    });
});

test('Should call ChapterDB.deleteChapter and delete a chapter', () => {
    const mock = jest.spyOn(ChapterDB, 'deleteChapter');

    return newChapter.deleteChapter().then(data => {
        expect(ChapterDB.deleteChapter).toHaveBeenCalled();
        expect(data).toBe(true);
        expect(ChapterDB.chapters).toEqual(expect.not.objectContaining(newChapter));
    });
});