const Course = require('../../model/course');
const CourseDB = require('../../model/database/courseDB');
jest.mock('../../model/database/courseDB');
let abstractExercise = {id: 1, idCourse: 1};
let newCourse = new Course(2, 1, "Deux cours", "Un contenu", 2);

test('Should call CourseDB.getAllCourses and return a list of Courses', () => {
    const mock = jest.spyOn(CourseDB, 'getAllCourses');

    return Course.getAllCourses().then(data => {
        expect(CourseDB.getAllCourses).toHaveBeenCalled();
        expect(data).toStrictEqual([Object.assign(new Course(),CourseDB.courses[0])]);
    })
});

test('Should add all the exercises of the course to it', () =>{
    let course = Object.assign(new Course(), CourseDB.courses[0]);
    course.exercises = course.getMyExercises([abstractExercise]);
    
    expect(course.exercises).toContain(abstractExercise);
});

test('Should call CourseDB.addCourse and add a course', () => {
    const mock = jest.spyOn(CourseDB, 'addCourse');

    return newCourse.addCourse().then(data => {
        expect(CourseDB.addCourse).toHaveBeenCalled();
        expect(data).toBe(newCourse);
        expect(CourseDB.courses).toContain(newCourse);
    });
});

test('Should call CourseDB.updateCourse and update a course', () => {
    const mock = jest.spyOn(CourseDB, 'updateCourse');
    
    newCourse.title = "Troisième chapitre";
    return newCourse.updateCourse().then(data => {
        expect(CourseDB.updateCourse).toHaveBeenCalled();
        expect(data).toBe(newCourse);
        expect(CourseDB.courses).toContain(newCourse);
    });
});

test('Should call CourseDB.deleteCourse and delete a course', () => {
    const mock = jest.spyOn(CourseDB, 'deleteCourse');

    return newCourse.deleteCourse().then(data => {
        expect(CourseDB.deleteCourse).toHaveBeenCalled();
        expect(data).toBe(true);
        expect(CourseDB.courses).toEqual(expect.not.objectContaining(newCourse));
    });
});