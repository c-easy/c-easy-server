const Do = require('../../model/do');
const DoDB = require('../../model/database/doDB');
const UserDB = require('../../model/database/userDB');
jest.mock('../../model/database/doDB');
jest.mock('../../model/database/userDB');

let newDo = new Do(1, 2, new Date(), null, 0, 0, null);

test('Should call DoDB.getAllDos and return all dos', () => {
    const mock = jest.spyOn(DoDB, 'getAllDosFromUser');

    let result = [];
    DoDB.dos.forEach(d => {
        result.push(Object.assign(new Do, d));
    });

    return Do.getAllDosFromUser({id: 1}).then(data => {
        expect(DoDB.getAllDosFromUser).toHaveBeenCalled();
        expect(data).toStrictEqual(result);
    });
});

test('Should call DoDB.getDoFromUserFromExercise and return a do', () => {
    const mock = jest.spyOn(DoDB, 'getDoFromUserFromExercise');

    return Do.getDoFromUserFromExercise(1, {id: 1}).then(data => {
        expect(DoDB.getDoFromUserFromExercise).toHaveBeenCalled();
        expect(data).toStrictEqual(Object.assign(new Do(), DoDB.currentDo));
    });
});

test('Should call DoDB.addDo and add a do', () => {
    const mock = jest.spyOn(DoDB, 'addDo');

    return newDo.addDo().then(data => {
        expect(DoDB.addDo).toHaveBeenCalled();
        expect(data).toBe(newDo);
    });
});

test('Should call DoDB.updateDo and update a do', () => {
    const mock = jest.spyOn(DoDB, 'updateDo');

    newDo.try++;
    return newDo.updateDo().then(data => {
        expect(DoDB.updateDo).toHaveBeenCalled();
        expect(data).toBe(newDo);
    });
});