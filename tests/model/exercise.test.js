const Exercise = require('../../model/exercise');
const ExerciseDB = require('../../model/database/exerciseDB');
jest.mock('../../model/database/exerciseDB');

let newExercise = new Exercise(2, 1);

test('Should call ExerciseDB.getAllExercises and return a list of Exercises', () => {
    const mock = jest.spyOn(ExerciseDB, 'getAllExercises');

    return Exercise.getAllExercises().then(data => {
        expect(ExerciseDB.getAllExercises).toHaveBeenCalled();
        expect(data).toStrictEqual([Object.assign(new Exercise(),ExerciseDB.exercises[0])]);
    })
});

test('Should call ExerciseDB.addExercise and add a exercise', () => {
    const mock = jest.spyOn(ExerciseDB, 'addExercise');

    return newExercise.addExercise().then(data => {
        expect(ExerciseDB.addExercise).toHaveBeenCalled();
        expect(data).toBe(newExercise);
        expect(ExerciseDB.exercises).toContain(newExercise);
    });
});

test('Should call ExerciseDB.updateExercise and update a exercise', () => {
    const mock = jest.spyOn(ExerciseDB, 'updateExercise');
    
    newExercise.title = "Troisième chapitre";
    return newExercise.updateExercise().then(data => {
        expect(ExerciseDB.updateExercise).toHaveBeenCalled();
        expect(data).toBe(newExercise);
        expect(ExerciseDB.exercises).toContain(newExercise);
    });
});

test('Should call ExerciseDB.deleteExercise and delete a exercise', () => {
    const mock = jest.spyOn(ExerciseDB, 'deleteExercise');

    return newExercise.deleteExercise().then(data => {
        expect(ExerciseDB.deleteExercise).toHaveBeenCalled();
        expect(data).toBe(true);
        expect(ExerciseDB.exercises).toEqual(expect.not.objectContaining(newExercise));
    });
});