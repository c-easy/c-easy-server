const User = require('../../model/user');
const UserDB = require('../../model/database/userDB');
jest.mock('../../model/database/userDB');

let newUser = new User("b17004949", "Mickaël BRADES", 0, 2020);

test('Should call UserDB.getUser and return a user', () => {
    const mock = jest.spyOn(UserDB, 'getUser');
    mock.mockImplementation((user) => Promise.resolve(user));

    let user = new User("b14009868", "Loïc BAYOL", 1, 2020);
    return User.getUser(user).then(data => {
        expect(UserDB.getUser).toHaveBeenCalled();
        expect(data).toStrictEqual(user);
    });
});

test('Should call UserDB.getAllUsersFromPromo and return a list of users', () => {
    const mock = jest.spyOn(UserDB, 'getAllUsersFromPromo');

    return User.getAllUsersFromPromo(2020).then(data => {
        expect(UserDB.getAllUsersFromPromo).toHaveBeenCalled();
        expect(data.length).toStrictEqual(1);
        expect(data).toContainEqual(UserDB.users[0]);
    });
});

test('Should call UserDB.getAllPromos and return a list of promos', () => {
    const mock = jest.spyOn(UserDB, 'getAllPromos');

    return User.getAllPromos().then(data => {
        expect(UserDB.getAllPromos).toHaveBeenCalled();
        expect(data.length).toStrictEqual(2);
        expect(data).toStrictEqual([2020,2019]);
    });
});

test('Should call UserDB.addUser and add a user', () => {
    const mock = jest.spyOn(UserDB, 'addUser');

    return newUser.addUser().then(data => {
        expect(UserDB.addUser).toHaveBeenCalled();
        expect(data).toBe(newUser);
        expect(UserDB.users).toContain(newUser);
    });
});

test('Should call UserDB.deleteUser and delete a user', () => {
    const mock = jest.spyOn(UserDB, 'deleteUser');

    return newUser.deleteUser().then(data => {
        expect(UserDB.deleteUser).toHaveBeenCalled();
        expect(data).toBe(true);
        expect(UserDB.users).toEqual(expect.not.objectContaining(newUser));
    });
});