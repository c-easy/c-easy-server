const Token = require("../../server/token");
const jwt = require('jsonwebtoken');

test('Should return a JWT Token using jwt.sign', () => {
    jest.spyOn(jwt, 'sign').mockReturnValueOnce("un essai");
    expect(Token.createToken("b14009868", "Loïc Bayol", 1)).toBe("un essai");
    
});

test('Should check if the token is valid and return its value', () => {
    let user = {
        id: "b14009868",
        name: "Loïc Bayol",
        type: 1
    };
    let token = Token.createToken(user.id, user.name, user.type);
    return Token.checkValidity(token).then(data => {
        expect(data.name).toBe(user.name);
        expect(data.id).toBe(user.id);
        expect(data.type).toBe(user.type);
        expect(data).toHaveProperty("exp");
        expect(data).toHaveProperty("iat");
    });
});


test('Should check if the token is valid and fail', () => {
    let user = {
        id: "b14009868",
        name: "Loïc Bayol",
        type: 1
    };
    jest.spyOn(jwt, 'sign').mockReturnValueOnce("un essai");
    let token = Token.createToken(user.id, user.name, user.type);
    return Token.checkValidity(token).catch(data => {
        expect(data).toBeInstanceOf(jwt.JsonWebTokenError);
        expect(data).toStrictEqual(new jwt.JsonWebTokenError("jwt malformed"));
    });
});